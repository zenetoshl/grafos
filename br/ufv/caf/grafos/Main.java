package br.ufv.caf.grafos;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
	Grafo g = new Grafo("grafoteste.txt");
	g.imprimeMatriz();
	System.out.println("  " + g.getVizinhosVertice(0));
    }
}
