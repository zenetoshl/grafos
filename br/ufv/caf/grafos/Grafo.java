package br.ufv.caf.grafos;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

public class Grafo {
    private double matrizValores[][];
    private int ordem;
    private int tamanho;

    public Grafo(String nome) throws IOException {
        int v1, v2;
        double peso;
        Scanner s = new Scanner(new File(nome));

        this.tamanho = s.nextInt();
        this.ordem = 0;

        matrizValores = new double[tamanho][tamanho];

        for(int i = 0; i < matrizValores.length; i++){
            for(int j = 0; j < matrizValores[i].length; j++){
                matrizValores[i][j] = 0;
            }
        }

        while (s.hasNext()) {
            this.ordem++;
            v1 = s.nextInt();
            v2 = s.nextInt();
            peso = s.nextDouble();

            matrizValores[v1 - 1][v2 - 1] = peso;
            matrizValores[v2 - 1][v1 - 1] = peso;
        }
    }

    public LinkedList<Integer> getVizinhosVertice(int indice){
        LinkedList<Integer> lista = new LinkedList<>();
        for(int i = 0; i < tamanho; i++){
            if(matrizValores[indice][i] != 0){
                lista.add(i);
            }
        }
        return lista;
    }

    public int getOrdemVertice(int indice){
        int contador = 0;
        for(int i = 0; i < tamanho; i++){
            if(matrizValores[indice][i] != 0){
                contador++;
            }
        }
        return contador;
    }

    public int getOrdem() { return ordem; }

    public int getTamanho() { return tamanho; }

    public void imprimeMatriz(){
        for (int i = 0; i < tamanho; i++){
            for (int j = 0; j < tamanho; j++) {
                System.out.printf(" %5.2f", matrizValores[i][j]);
            }
            System.out.println();
        }
    }
}
